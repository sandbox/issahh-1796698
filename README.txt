***************************************************************
* This module is very easy to use!                            *
* But please make sure to enable the AWS Cloud module         *
* So through it you can connect to you AWS account.           *
*                                                             *
* Go to Clouds -> Configure AWS Clouds -> Add Cloud           *
* And you can connect from there.                             *
*                                                             *
* After that you can see your account EBS Volumes in          *
* admin/settings/aws_ebs_backups and from there you can       *
* Schedule snapshots.                                         *
*                                                             *
* By issahh.                                                  *
***************************************************************
